import {cleanConsole, createAll} from './data';
import {formatData} from './example-1';
const _ = require('lodash');

const companies = createAll();

cleanConsole(3, companies);

console.log('---- EXAMPLE 3 --- ', companies);

function upperCase(word) {
  if (!word) return;
  else return word.toUpperCase();
}

function transformValue(companies) {
  companies.map((element) => {
    Object.entries(element).forEach(([key, value]) => {
      if (key === 'name') element[key] = upperCase(value);
      if (key === 'users') {
        value.map((el) => {
          Object.entries(el).forEach(([k, v]) => {
            if (k === 'firstName' || k === 'lastName') el[k] = upperCase(v);
          });
        });
      };
    });
  });
  return companies;
}

function isUpperCase(companies) {
  return companies.every((element) => {
    if (element.name === element.name.toUpperCase()) return true;
    else if (element.users) {
      element.users.map((el) => {
        const fn = el.firstName;
        const ln = el.lastName;
        if (fn === fn.toUpperCase() && ln === ln.toUpperCase()) {
          return true;
        } else return false;
      });
    } else return false;
  });
};

const companiesList = _.cloneDeep(companies);
const companiesListUpperCase = _.cloneDeep(companies);

console.group('---- Result 3 --- ');
console.log('---- Companies example-1 --- ', isUpperCase(formatData(companiesList)));
console.log((formatData(companiesList)));
console.log('---- Companies example-1 modified --- ', isUpperCase(transformValue(formatData(companiesListUpperCase))));
console.log(transformValue(formatData(companiesListUpperCase)));
console.groupEnd();
// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Cree una función tomando la variable "companies" como parámetro y devolviendo
// un booleano que valida que todos los nombres de las empresas y los atributos
// "firstName" y "lastName" de "users" están en mayúsculas.
// Debes probar la operación de esta función importando la función creada
// en el "example-1.js".

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the "companies" variable as a parameter and returning
// a boolean validating that all the names of the companies and the attributes "firstName"
// and "lastName" of "users" are capitalized. You must test the operation
// of this function by importing the function created for "example-1.js"
