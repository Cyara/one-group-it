import {createAll, cleanConsole} from './data';
const companies = createAll();

cleanConsole(1, companies);

console.log('---- EXAMPLE 1 --- ', companies);

/**
 * ## Capitalize Method
 *
 * Método para agregar mayúscula inicial a determinada cadena de texto.
 * @param {string} word - cadena
 * @example
 * 'apple' => 'Apple'
 * @return {string}
 */
function capitalize(word) {
  if (!word) return;
  else return word.split('')[0].toUpperCase() + word.slice(1);
}

/**
 * ## Complementing Keys Method
 *
 * Método para complementar llaves faltantes
 * @param {Array} list
 */
function complementingKeys(list) {
  list.map((element) => {
    Object.entries(element).forEach(([key, value]) => {
      if (value === undefined) {
        element[key] = ' ';
      };
    });
  });
}

/**
 * ## Order by number of users method
 *
 * Método que se encarga de organizar los elementos
 * de un objeto a partir de la cantidad de usuario.
 * @param {Object} elementA
 * @param {Object} elementB
 * @return {Boolean}
 */
function orderByNumberOfUsers(elementA, elementB) {
  if (elementA.users.length < elementB.users.length) {
    return 1;
  } else if (elementA.users.length > elementB.users.length) {
    return -1;
  } else return 0;
}

/**
 * ## Ordering for users method
 *
 * Método que ordena los usuarios por orden alfabético
 * @param {Object} a
 * @param {Object} b
 * @return {Boolean}
 */
function orderingForUsers(a, b) {
  if (a.firstName.toUpperCase() < b.firstName.toUpperCase()) {
    return -1;
  } else if (a.firstName.toUpperCase() > b.firstName.toUpperCase()) {
    return 1;
  } else return 0;
}

/**
 * ## Format data method
 *
 * Metodo que se encarga de agregar mayúscula inicial a nombre, firstName y lastName,
 * validando a su vez el orden del objeto por cantidad de usuarios de manera
 * desendente y organizando la lista de usuarios por orden alfabético.
 * @param {Array} companies
 * @return {Array}
 */
export function formatData(companies) {
  companies.map((element) => {
    Object.entries(element).forEach(([key, value]) => {
      if (key === 'name') element[key] = capitalize(value);
      if (key === 'users') {
        complementingKeys(value);
        value = value.sort(orderingForUsers);
        value.map((el) => {
          Object.entries(el).forEach(([k, v]) => {
            if (k === 'firstName' || k === 'lastName') el[k] = capitalize(v);
          });
        });
      };
    });
  });
  companies = companies.sort(orderByNumberOfUsers);
  return companies;
};

const companiesList = companies.map((el) => Object.assign({}, el));

console.log('---- RESULT 1 --- ', formatData(companiesList));
// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando la variable "companies" como parámetro y reemplazando
// todos los valores "undefined" en "usuarios" por un string vacío.
// El nombre de cada "company" debe tener una letra mayúscula al principio, así como
// el apellido y el nombre de cada "user".
// Las "companies" deben ordenarse por su total de "user" (orden decreciente)
// y los "users" de cada "company" deben aparecer en orden alfabético.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the variable "companies" as a parameter and replacing
// all values "undefined" in "users" by an empty string.
// The name of each "company" must have a capital letter at the beginning as well as
// the last name and first name of each "user".
// The "companies" must be sorted by their number of "user" (decreasing order)
// and the "users" of each "company" must be listed in alphabetical order
