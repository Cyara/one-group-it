import {cleanConsole, createAll} from './data';
const _ = require('lodash');

const companies = createAll();

cleanConsole(7, companies);
console.log('---- EXAMPLE 7 part 1 --- ', companies);
console.log('---- EXAMPLE 7 part 2 --- ', companies);
console.log('---- EXAMPLE 7 part 3 --- ', companies);
console.log('---- EXAMPLE 7 part 4 --- ', companies);
console.log('---- EXAMPLE 7 part 5 --- ', companies);
console.log('---- EXAMPLE 7 part 6 --- ', companies);
console.log('---- EXAMPLE 7 part 7 --- ', companies);
console.log('---- EXAMPLE 7 part 8 --- ', companies);
console.log('---- EXAMPLE 7 part 9 --- ', companies);

function partFirst(id) {
  const company = companies.find((el) => el.id === id);
  return company.name;
}

function partSecond(id) {
}

function partFourth(id) {
  const company = companies.find((el) => el.id === id);
  const user = {
    firstName: 'Juan',
    lastName: 'Delgado',
    age: 35,
    id: company.usersLength +1,
    car: true,
  };
  const users = [...company.users, user];
  const usersLength = users.length;
  const companyUpdate = Object.assign(company, {users: users}, {usersLength: usersLength});
  return companyUpdate;
}

function partFifth(id, payload) {
  const company = companies.find((el) => el.id === id);
  const updateCompany = Object.assign({company}, {
    name: payload.name,
    users: payload.users,
    isOpen: payload.isOpen,
    usersLength: company.usersLength + 30,
    id: company.id+30,
  });
  return updateCompany;
}

const dataPut ={
  name: 'Empresa',
  isOpen: true,
  usersLength: 10,
};

function partSixth(id, idUSer) {
  const company = companies.find((el) => el.id === id);
  const users = company['users'].filter((item) => item.id !== idUSer);
  const usersLength = users.length;
  return Object.assign(company, users, usersLength);
}

function partSeventh(id, idUSer) {
  const company = companies.find((el) => el.id === id);
  const users = company['users'].filter((item) => item.id !== idUSer);
  const usersLength = users.length;
  return Object.assign(company, users, usersLength);
}

const dataPutEighth ={
  firstName: 'Juan',
  lastName: 'Cortéz',
  age: 45,
  id: 0,
  car: true,
};

function partEighth(id, payload) {
  const company = companies.find((el) => el.id === id);
  const findUser = company['users'].filter((item) => item.id !== payload.id);
  const userData = {
    firstName: payload.firstName,
    lastName: payload.lastName,
    age: payload.age,
    id: payload.id,
    car: payload.car,
  };
  const users = [...findUser, userData];
  const usersLength = users.length;
  Object.entries(company).forEach(([k, v]) => {
    if (k == 'users') company['users'] = [...findUser, userData];
    if (k ==='usersLength') company['usersLength'] = usersLength;
  });
  return company;
}

function core(companies, option) {
  switch (option) {
    case 'part-1':
      const p1 = companies.map((el) => partFirst(el.id));
      return p1;
    case 'part-2':
      const p2 = companies.map((el) => partSecond(el.id));
      return p2;
    case 'part-4':
      const p4 = companies.map((el) => partFourth(el.id));
      return p4;
    case 'part-5':
      const p5 = companies.map((el) => partFifth(el.id, dataPut));
      return p5;
    case 'part-6':
      const p6 = companies.map((el) => partSixth(el.id, 0));
      return p6;
    case 'part-7':
      const p7 = companies.map((el) => partSeventh(el.id, 0));
      return p7;
    case 'part-8':
      const p8 = companies.map((el) => partEighth(el.id, dataPutEighth));
      return p8;
  }
}

console.group('---- RESULT 7 ---');
console.log('---- part 1 --- ', core(_.cloneDeep(companies), 'part-1'));
console.log('---- part 2 --- ', core(_.cloneDeep(companies), 'part-2'));
console.log('---- part 4 --- ', core(_.cloneDeep(companies), 'part-4'));
console.log('---- part 5 --- ', core(_.cloneDeep(companies), 'part-5'));
console.log('---- part 6 --- ', core(_.cloneDeep(companies), 'part-6'));
console.log('---- part 7 --- ', core(_.cloneDeep(companies), 'part-7'));
console.log('---- part 8 --- ', core(_.cloneDeep(companies), 'part-8'));
console.groupEnd();
// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Parte 1: Crear una función tomando como parámetro un "id" de "company" y
// devolviendo el nombre de esta "company".

// Parte 2: Crear una función tomando como parámetro un "id" de "company" y
// quitando la "company" de la lista. -- No entiendo

// Parte 3: Crear una función tomando como parámetro un "id" de "company" y
// permitiendo hacer un PATCH (como con una llamada HTTP) en todos los
// atributos de esta "company" excepto en el atributo "users".

// Parte 4: Crear una función tomando como parámetro un "id" de "company" y un
// nuevo "user" cuyo el apelido es "Delgado", el nombre "Juan", de 35 años y
// dueño de un carro. El nuevo "user" debe agregarse a la lista de "users" de este
// "company" y tener un "id" generado automáticamente. La función también debe modificar
// el atributo "usersLength" de "company".

// Parte 5: Crear una función tomando como parámetro un "id" de "company" y
// permitiendo hacer un PUT (como con una llamada HTTP) en esta "company" excepto
// en el atributo "users".

// Parte 6: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user". La función debe quitar este "user" de la lista de "users"
// de "company" y cambiar el atributo "usersLength" de "company".

// Parte 7: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user" que permite hacer un PATCH (como con una llamada HTTP) en este
// "user".

// Parte 8: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user" que permite hacer un PUT (como con una llamada HTTP) en este
// "user".

// Parte 9: Crear una función tomando como parámetro dos "id" de "company" y
// un "id" de "user". La función debe permitir que el user sea transferido de la
// primera "company" a la segunda "company". El atributo "usersLength" de cada
// "company" debe actualizarse.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Part 1: Create a function taking as parameter an "id" of "company" and
// returning the name of this "company".

// Part 2: Create a function taking as parameter an "id" of "company" and
// removing the "company" from the list.

// Part 3: Create a function taking as a parameter an "id" of "company" and
// allowing to make a PATCH (as with an HTTP call) on all
// attributes of this "company" except on the "users" attribute.

// Part 4: Create a function taking as parameter an "id" of "company" and a
// new "user" whose name is "Delgado", the first name "Juan", aged 35 and
// a car. The new "user" must be added to the "users" list of this
// "company" and have an automatically generated "id". The function must also modify
// the "usersLength" attribute of "company".

// Part 5: Create a function taking as parameter an "id" of "company" and
// allowing to make a PUT (as with an HTTP call) on this "company" except
// on the "users" attribute.

// Part 6: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user". The function must remove this "user" from the list of "users"
// from "company" and change the attribute "usersLength" from "company".

// Part 7: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user" allowing to make a PATCH (as with an HTTP call) on this
// "user".

// Part 8: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user" allowing to make a PUT (as with an HTTP call) on this
// "user".

// Part 9: Create a function taking as parameter two "id" of "company" and
// an "id" of "user". The function must allow the user to be transferred as a parameter
// from the 1st "company" to the 2nd "company". The "usersLength" attribute of each
// "company" must be updated
