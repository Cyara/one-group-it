import {cleanConsole, createAll} from './data';
import {createTables} from './example-4';

const _ = require('lodash');
const companies = createAll();

cleanConsole(6, companies);

console.log('---- EXAMPLE 6 --- ', companies);

function newObject(companies) {
  const newObject = companies.map((el) => {
    const f = el.fistName;
    const l = el.lastName;
    const a = el.age;
    const key = [((f ? f : '') + (l ? l: '') + (a ? a.toString() :'0')).trim()];
    return {
      [key]: el.car,
    };
  });
  const averageWithCar = newObject.map((el) => el).reduce((p, c) => Object.assign(p, c));
  return averageWithCar;
}

const companiesList = _.cloneDeep(companies);

console.log('---- RESULT 6 --- ', newObject(createTables(companiesList)));
// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Cree una función tomando la variable "companies" como parámetro y devolviendo
// un nuevo objeto cuyos atributos son la concatenación del apelido, nombre y
// edad de cada "user". Cada atributo debe tener el valor de boolean "car".
// Ver ejemplo a continuación.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the "companies" variable as a parameter and returning
// a new object whose attributes are the concatenation of the name, first name and
// the age of each user. Each attribute must have the value of boolean "car".
// See example below

const example = {
  johnDoe32: true,
  BernardoMinet45: false,
  alinaChef23: true,
};

console.log(example);
