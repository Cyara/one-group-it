import {cleanConsole, createAll} from './data';
const _ = require('lodash');
const companies = createAll();

cleanConsole(4, companies);

console.log('---- EXAMPLE 4 --- ', companies);

/**
 * ## Ordering for users method
 *
 * Método que ordena los usuarios por orden edad
 * @param {Object} a
 * @param {Object} b
 * @return {Boolean}
 */
function orderingForUsers(a, b) {
  if (a.age < b.age) return 1;
  else if (a.age > b.age) return -1;
  else return 0;
}

/**
 * ## Create tables method
 *
 * Método que se encarga de crear el nuevo objeto
 * agragando el nuevo campo 'company' a cada usuario y
 * creando un array de objetos únicamente de usuarios
 * organizados por la edad.
 * @param {Object} companies
 * @return {Array}
 */
export function createTables(companies) {
  const dataTable = companies.map((element) => {
    element.users.forEach((el) => {
      el['company'] = element.name;
    });
    return element.users;
  });
  const dataTableCompleted = dataTable.flat().sort(orderingForUsers);
  return dataTableCompleted;
}

const companiesList = _.cloneDeep(companies);

console.log('---- RESULT 4 --- ', createTables(companiesList));
// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando como parámetro la variable "companies" y agrupando
// todos los "users" de todas las "companies" en una sola tabla. Cada "user"
// debe tener un nuevo atributo "company" que tenga como valor el nombre de la
// dicha "company". Los "users" deben ordenarse de acuerdo con sus edad
// (de mayor a menor).

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking as parameter the "companies" variable and grouping
// all "users" of all "companies" in a single table. Each "user"
// must have a new attribute "company" having for value the name of the "company"
// to which it belongs. The "users" must be sorted according to their
// age (from oldest to youngest)
